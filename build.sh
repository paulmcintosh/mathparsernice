clear
rm -R Build/
mkdir Build
cd App/Core/
cp *.cs ../../Build/
cd ../Controllers/
cp *.cs ../../Build/
cd ../Models/
cp *.cs ../../Build/
cd ../../Build
mcs -out:App.exe *.cs
mono App.exe
