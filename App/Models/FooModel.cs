using System;

class FooModel 
{

	string txt;

	string tag;

	public object Init(DataModel data,object par1)
	{

		data.Set("a","+");

		data.Set("b","-");

		data.Set("c","*");

		data.Set("d","/");

		data.Set("e","(");

		data.Set("f",")");

		StringToCharArray(data, par1);

		return data;
	}

	public void StringToCharArray(DataModel data,object par1){

		txt = par1.ToString();

		char[] charArr = txt. ToCharArray();

		foreach (char ch in charArr){

			tag = ch.ToString();

			data.Get(tag);


		}

		data.Formula();

	}

}
