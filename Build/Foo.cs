using System;

class Foo : Controller
{
	public void Bar(object par1, object par2)
	{		

		DataModel data = new DataModel();
		
		FooModel foo = new FooModel();

		foo.Init(data,par1);
		
		Model();

		View();
	}

}// end of Foo
