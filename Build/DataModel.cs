using System;
using System.Collections;
using System.Collections.Generic;

public class DataModel
{

	public string txt;

	public List<Data> list = new List<Data>();

	public int tracker;

	public void Get(String tag)
	{		
		foreach (var item in list)
		{
			Detect(item.key,item.value,tag);
		}
	
	}

	public void Formula(){

		System.Console.WriteLine("Formula:{0}",txt);

	}

	public void Detect(String key, String value, String tag){

		//TODO: Detect operator
		if(key == tag){

			txt += value;

		} else {

			txt += tag;

		}

	}
	
	public void Set(String k, String v)
	{

		list.Add(new Data() { 
				key = k,
				value = v 
			}
		);
		
		tracker++;

	}

}
