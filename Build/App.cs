using System;
using System.Reflection;

class App
{

	static object[] parametersArray = new object[] { "3a2c4","test"};

	static void Main()
	{
		controller("Foo","Bar");

		parametersArray[0] = "32a2d2";

		controller("Foo","Bar");

		parametersArray[0] = "500a10b66c32";

		controller("Foo","Bar");

	}

	static void controller(String myclass, String mymethod)
	{
		Type type = Type.GetType(myclass);
		Object obj = Activator.CreateInstance(type);
		MethodInfo methodInfo = type.GetMethod(mymethod);
		methodInfo.Invoke(obj, parametersArray);
	}
}// end of Program



